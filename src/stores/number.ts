import { ref } from "vue";
import { defineStore } from "pinia";
import type Number from "@/type/number";
export const useNumberStore = defineStore("number", () => {
  const nums = ref<Number[]>([{ id: 1, num: 0 }]);
  const multis = ref<Number[]>([{ id: 1, num: 0 }]);
  const increse = (id: number): void => {
    const index = nums.value.findIndex((item) => item.id === id);
    nums.value[index].num++;
  };
  const multiP = (id: number): void => {
    const index = nums.value.findIndex((item) => item.id === id);
    multis.value[index] = nums.value[index];
  };
  const decrement = (id: number): void => {
    const index = nums.value.findIndex((item) => item.id === id);
    if (nums.value[index].num > 0) {
      nums.value[index].num--;
    }
  };
  return { nums, multis, multiP, increse, decrement };
});
